# Ayvos OpenSource Cov19 Maske Tespit 

[![Ayvos](images/logo.jpg)](https://ayvos.com)

Tüm dünyada olduğu gibi ülkemizde de etkisini gösteren koronavirüs her geçen gün şiddetini arttırarak yayılmaktadır. Yayılma hızını azaltmak veya virüsün etkisini bitirmek toplumsal önlemler ile mümkündür. Bu konuda toplu alanlarda maske kullanımı önem arzetmektedir ve Cumhurbaşkanlığı tarafından bilim kurulu tavsiyesi üzerine zorunlu hale getirilmiştir.

Bu proje Opensource kaynaklardan faydanılarak Ayvos Bilgi Teknolojileri tarafından herkese açık şekilde yayınlanmaktadır.
## Gereksinimler!

Aşağıdaki gereksinimler test edilen ve başarılı şekilde çalışan sistem bilgilerini içermektedir. Test aşamasında 1050 Ti ve Rtx 2080 ekran kartları kullanılmıştır.

  * Cuda 10
  * Cudnn 7.6.4
  * mxnet-cu100==1.5.1
  * numpy==1.17.4
  * Cython==0.29.15
  * opencv-python==4.2.0.34


## Demo
Demo tek resim üzerinden çalışmaktadır. Resmin yolunu ve thresholdları kod içerisinden değiştirebilirsiniz. Çıktı görsel "cov_test.jpg" olarak klasöre yazılmaktadır.
```sh
    python3 covmaskdetect.py
```
   [![Ayvos](images/cov_test.jpg)](https://ayvos.com)


## Ayvos Cov19 Maske Free Api
Ayvos Bilgi Teknolojileri tarafından bu çalışmaya ek olarak kişi tespiti ve sayımı Free Api olarak kullanıcılara sunulmaktadır. Uygulama test sunucularımızda bulunduğundan dolayı ücretsiz talep üzerine api erişimi sağlanmaktadır. Api authentication gerektirmektedir. info@ayvos.com e-posta adresi üzerinden ücretsiz talepte bulunabilirsiniz.
#### Request 
```
    POST http://corona.ayvos.com:5005/maskdetection
    Content-Type: application/json
    Body:
    {
	"url":"https://www.izmirsondakika.com/wp-content/uploads/2020/04/maske-takmayana-ceza.jpg",
	"faceThresh":0.75,
	"personThresh":0.65
    }
    
```
#### Response
```
{
    "result_face": {
        "0": {
            "probability": 0.9973804354667664,
            "status": "masked",
            "x1": 273,
            "x2": 361,
            "y1": 38,
            "y2": 155
        },
        "1": {
            "probability": 0.7820274233818054,
            "status": "masked",
            "x1": 416,
            "x2": 486,
            "y1": 37,
            "y2": 127
        },
        "2": {
            "probability": 0.9972910284996033,
            "status": "masked",
            "x1": 519,
            "x2": 616,
            "y1": 25,
            "y2": 150
        }
    },
    "result_person": {
        "0": {
            "probability": 0.9960152506828308,
            "x1": 133,
            "x2": 38,
            "y1": 437,
            "y2": 431
        },
        "1": {
            "probability": 0.9900494813919067,
            "x1": 358,
            "x2": 17,
            "y1": 692,
            "y2": 436
        },
        "2": {
            "probability": 0.9886611700057983,
            "x1": 366,
            "x2": 12,
            "y1": 513,
            "y2": 201
        }
    },
    "totalFace": 3,
    "totalMasked": 3,
    "totalPerson": 3,
    "totalUnmasked": 0
}
```

## Referanslar
```
https://github.com/deepinsight/insightface/tree/master/RetinaFaceAntiCov

@inproceedings{deng2019retinaface,
title={RetinaFace: Single-stage Dense Face Localisation in the Wild},
author={Deng, Jiankang and Guo, Jia and Yuxiang, Zhou and Jinke Yu and Irene Kotsia and Zafeiriou, Stefanos},
booktitle={arxiv},
year={2019}
}

```
## İletişim
Sorularınız ve önerileriniz için bize info@ayvos.com e-posta adresimizinden ulaşabilirsiniz.





